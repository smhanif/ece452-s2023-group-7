| Date       | Athi | Haifan | Huanyou | Saifullah | Shlok | Task                                |
|------------|------|--------|---------|-----------|-------|-------------------------------------|
| 2023/08/01 | 2    | 5      | 4       | 3         | 7     | Deliverable 7                       |
| 2023/07/31 |      | 2      |         |           |       | Tried to publish app to Google Play |
| 2023/07/28 |      | 5      |         |           |       | Demo Video                          |
| 2023/07/28 | 2    | 10     | 2       | 2         | 13    | Deliverable 6                       |
| 2023/07/27 |      |        |         | 3         |       | Final QA and code changes           |
| 2023/07/26 | 1    | 2      | 1       | 1         | 1     | Record demo                         |
| 2023/07/26 | 1    | 2      | 1       | 1         | 3     | General refactoring                 |
| 2023/07/26 |      |        |         | 1         | 3     | Back button functionality           |
| 2023/07/25 |      |        |         | 1         |       | Plan demo                           |
| 2023/07/25 | 2    |        |         | 4         | 5     | Bug fixes before demo               |
| 2023/07/25 |      |        |         | 5         | 3     | Frontend design tweaks              |
| 2023/07/25 | 4    |        |         | 0.5       |       | Extract/display fundamentals        |
| 2023/07/25 |      |        |         | 3         |       | Stock fundamentals frontend         |
| 2023/07/25 |      |        |         | 4         |       | DB schema change to add bio/links   |
| 2023/07/25 |      |        |         | 5         |       | Redesign survey questions           |
| 2023/07/24 | 0.5  |        | 3       |           | 1     | Implement common followers/Stocks   |
| 2023/07/24 | 10   |        |         |           | 1     | Following/unfollowing functions     |
| 2023/07/24 |      | 0.5    |         |           |       | Async invocation for some methods   |
| 2023/07/24 |      | 1      |         |           |       | Fix DELETE on watchlistItem table   |
| 2023/07/24 |      |        |         | 2         | 5     | Frontend design changes             |
| 2023/07/23 | 2    |        |         |           | 1     | Validations + bugs                  |
| 2023/07/23 | 2    |        |         |           | 5     | Implementing modal                  |
| 2023/07/22 | 5    |        |         |           |       | User adapter logic                  |
| 2023/07/22 |      | 0.5    |         |           | 1     | Manual testing                      |
| 2023/07/21 | 5    |        |         |           |       | Refactor + add actions to db        |
| 2023/07/21 |      | 2      |         |           |       | API to get all records from a table |
| 2023/07/20 | 3    |        |         | 1         |       | News api calls                      |
| 2023/07/20 |      |        | 3       |           |       | Store username in sharedPref        |
| 2023/07/20 |      | 4      |         |           |       | Adjust query & actions table schema |
| 2023/07/19 | 10   |        |         | 2         |       | Price data api + watchlist          |
| 2023/07/19 | 10   |        |         |           |       | Price data api + watchlist          |
| 2023/07/19 |      |        | 1       |           |       | Use salt to hash user password      |
| 2023/07/19 |      |        | 2       |           |       | Handle SignIn/SignUp edgecases      |
| 2023/07/19 |      | 3.5    |         |           |       | Adjust query & user table schema    |
| 2023/07/19 |      | 2      |         |           |       | Adjust query to watchlistItem table |
| 2023/07/19 |      | 3      |         |           |       | Change cloud DB schema              |
| 2023/07/19 |      |        |         | 1         | 1     | Research SQLLite for potential use  |
| 2023/07/18 |      |        | 1       |           |       | Verify user sign in data.           |
| 2023/07/18 |      |        |         | 3         |       | Design watchlist rows               |
| 2023/07/18 |      |        |         | 1         |       | Use RecyclerView on Updates tab     |
| 2023/07/19 | 7    |        |         | 1         |       | User search page + misc             |
| 2023/07/19 |      |        |         | 1         | 3     | Planning and project management     |
| 2023/07/19 |      |        |         | 4         | 2     | Creating tickets to work on         |
| 2023/07/19 |      |        |         | 1         |       | Creating team Notion                |
| 2023/07/18 |      |        | 6       |           |       | Store user SignUp info in DB        |
| 2023/07/18 |      | 4      |         |           |       | Send API request from app           |
| 2023/07/18 | 0.5  | 0.5    | 0.5     | 0.5       | 10    | Deliverable 5                       |
| 2023/07/17 | 10   |        |         |           |       | Add stock fragment + other logic    |
| 2023/07/17 |      |        |         | 1.5       |       | Learn how to use RecyclerView       |
| 2023/07/16 |      |        |         | 3         |       | Design news updates.                |
| 2023/07/16 | 0.5  | 0.5    | 0.5     | 0.5       | 0.5   | Team meeting                        |
| 2023/07/16 |      | 3      |         |           |       | Publish to Play Store               |
| 2023/07/15 | 10   |        |         | 3         |       | External API R&D                    |
| 2023/07/15 | 2    |        |         |           |       | Field Validations                   |
| 2023/07/15 |      | 14     |         |           |       | Cloud Database Construction         |
| 2023/07/13 |      |        |         | 3.5       |       | Updates frontend.                   |
| 2023/07/12 |      |        |         | 2         |       | Create profile page for all users   |
| 2023/07/12 |      |        |         | 2         |       | Refactor profile into components.   |
| 2023/07/12 | 0.5  | 0.5    | 10      | 0.5       | 0.5   | Deliverable 4                       |
| 2023/06/28 |      |        |         | 8         |       | Refined Figma mocks                 |
| 2023/06/28 |      | 1.5    |         | 0.5       |       | Deliverable 3 Doc                   |
| 2023/06/28 | 0.5  | 0.5    | 0.5     | 0.5       | 0.5   | Deliverable 3 Presentation          |
| 2023/06/28 |      | 2      |         | 6         |       | Deliverable 3 Dev Work              |
| 2023/06/15 | 1    | 1      | 1       | 1         | 1     | Team meeting                        |
| 2023/06/12 | 1    | 3      | 1       | 0.5       | 1     | Deliverable 2                       |
| 2023/06/1  | 2    |        | 0.5     | 0.5       | 2     | Deliverable 1 Report                |
| 2023/05/30 | 1    |        | 0.5     | 1         | 1     | Deliverable 1 Presentation          |
| 2023/05/29 |      |        |         | 3         |       | Initial Figma mocks                 |
| 2023/05/25 | 1    |        | 1       | 1         | 1     | Brainstorming                       |
