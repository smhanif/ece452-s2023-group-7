const AWS = require("aws-sdk");
AWS.config.update({ region: 'ca-central-1' });
const dynamodb = new AWS.DynamoDB({ apiVersion: '2012-08-10' })

async function get(event) {
    // get the DynamoDB table name and the primary key from the parameter
    const tableName = event.queryStringParameters.table;
    var primaryKey, primaryKeyValue;
    switch (tableName) {
        case 'watchlistItem':
            if ('ticker' in event.queryStringParameters && 'userName' in event.queryStringParameters) {
                const params = {
                    TableName: tableName,
                    Key: {
                        'userName': { S: event.queryStringParameters.userName},
                        'ticker': { S: event.queryStringParameters.ticker}
                    }
                };
                return await dynamodb.getItem(params).promise().then((response) => {
                    const body = {
                        statusCode: '200',
                        body: JSON.stringify(AWS.DynamoDB.Converter.unmarshall(response.Item)),
                    }
                    return body;
                }, (error) => {
                    console.log(error)
                    return error;
                });
            } else {
                // use query instead of getItem
                const params = ('userName' in event.queryStringParameters) ? {
                    TableName: tableName,
                    KeyConditionExpression: 'userName = :userName',
                    ExpressionAttributeValues: {
                        ':userName': { S: event.queryStringParameters.userName }
                    }
                } : {
                    TableName: tableName,
                    IndexName: 'ticker-index',
                    KeyConditionExpression: 'ticker = :ticker',
                    ExpressionAttributeValues: {
                        ':ticker': { S: event.queryStringParameters.ticker }
                    }
                }
                return await dynamodb.query(params).promise().then((response) => {
                    var items = [];
                    response.Items.forEach((item) => {
                        items.push(AWS.DynamoDB.Converter.unmarshall(item))
                    });
                    const body = {
                        statusCode: '200',
                        body: JSON.stringify(items),
                    }
                    return body;
                }, (error) => {
                    console.log(error)
                    return error;
                });
            }
            // no need to break. previous returns get it all covered
        case 'user':
            if ('email' in event.queryStringParameters) {
                // query by email
                const params = {
                    TableName: tableName,
                    IndexName: 'email-index',
                    KeyConditionExpression: 'email = :email',
                    ExpressionAttributeValues: {
                        ':email': { S: event.queryStringParameters.email }
                    }
                }
                return await dynamodb.query(params).promise().then((response) => {
                    var items = [];
                    response.Items.forEach((item) => {
                        items.push(AWS.DynamoDB.Converter.unmarshall(item))
                    });
                    if (items.length == 0) {
                        items = {};
                    } else {
                        items = items[0];
                    }
                    const body = {
                        statusCode: '200',
                        body: JSON.stringify(items),
                    }
                    return body;
                }, (error) => {
                    console.log(error)
                    return error;
                });
            } else {
                // the request is by the primary key - userName
                // let it fall through to the default case
            }
        case 'actions':
            if (event.queryStringParameters.table == 'actions' &&
                'userName' in event.queryStringParameters) {
                // query by userName
                const params = {
                    TableName: tableName,
                    IndexName: 'userName-index',
                    KeyConditionExpression: 'userName = :userName',
                    ExpressionAttributeValues: {
                        ':userName': { S: event.queryStringParameters.userName }
                    }
                }
                return await dynamodb.query(params).promise().then((response) => {
                    var items = [];
                    response.Items.forEach((item) => {
                        items.push(AWS.DynamoDB.Converter.unmarshall(item))
                    });
                    const body = {
                        statusCode: '200',
                        body: JSON.stringify(items),
                    }
                    return body;
                }, (error) => {
                    console.log(error)
                    return error;
                });
            } else {
                // the request is by the primary key - actionID
                // let it fall through to the default case
            }
        default:
            for (var key in event.queryStringParameters) {
                // tableName is the table name, and the other parameter is the primary key
                if (key != 'table') {
                    primaryKey = key;
                    primaryKeyValue = event.queryStringParameters[key];
                }
            }
            const params = {
                TableName: tableName,
                Key: {
                    [primaryKey]: { S: primaryKeyValue }
                }
            };
            return await dynamodb.getItem(params).promise().then((response) => {
                const body = {
                    statusCode: '200',
                    body: JSON.stringify(AWS.DynamoDB.Converter.unmarshall(response.Item)),
                }
                return body;
            }, (error) => {
                console.log(error)
                return error;
            });
            // no need to break. previous returns get it all covered
    }
}

async function post(event) {
    // get the DynamoDB table name from the parameter
    const tableName = event.queryStringParameters.table;
    // get the body of the request
    const body = JSON.parse(event.body);
    // console.log(body)

    // create the DynamoDB put parameters
    const params = {
        TableName: tableName,
        Item: body
    };
    return dynamodb.putItem(params).promise().then((response) => {
        const body = {
            statusCode: '200',
            body: JSON.stringify(response.Item),
        }
        return body;
    }, (error) => {
        console.log(error)
        return error;
    });
}

async function put(event) {
    const tableName = event.queryStringParameters.table;
    const body = JSON.parse(event.body);
    const params = {
        TableName: tableName,
        Item: body
    }
    return dynamodb.putItem(params).promise().then((response) => {
        const body = {
            statusCode: '200',
            body: JSON.stringify(response.Item),
        }
        return body;
    }, (error) => {
        console.log(error)
        return error;
    });
}

async function del(event) {
    // get the DynamoDB table name and the primary key from the parameter
    var tableName, primaryKey, primaryKeyValue, sortKey, sortKeyValue;
    for (var key in event.queryStringParameters) {
        // tableName is the table name, and the other parameter is the primary key
        if (key != 'table') {
            primaryKey = key;
            primaryKeyValue = event.queryStringParameters[key];
        } else {
            tableName = event.queryStringParameters[key];
        }
    }
    if (tableName == 'watchlistItem') {
        sortKey = 'ticker';
        sortKeyValue = event.queryStringParameters.ticker;
        primaryKey = 'userName';
        primaryKeyValue = event.queryStringParameters.userName;
    }
    // create the DynamoDB delete parameters
    const params = {
        TableName: tableName,
        Key: (tableName == 'watchlistItem') ? {
          [primaryKey]: { S: primaryKeyValue },
          [sortKey]: { S: sortKeyValue}
        } : {
            [primaryKey]: { S: primaryKeyValue }
        }
    }
    return dynamodb.deleteItem(params).promise().then((response) => {
        const body = {
            statusCode: '200',
            body: JSON.stringify(response.Item),
        }
        return body;
    }, (error) => {
        console.log(error)
        return error;
    });
}

exports.handler = async (event, context) => {
  // Switch on the method
    switch (event.requestContext.http.method) {
        case 'GET':
            return get(event);
        case 'POST':
            return post(event);
        case 'PUT':
            return put(event);
        case 'DELETE':
            return del(event);
        default:
            // return JSON.stringify({ statusCode: 400, body: 'Unsupported method' });
            // return the entire event object for debugging
            return JSON.stringify(event);
    }
};