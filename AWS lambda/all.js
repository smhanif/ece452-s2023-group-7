const AWS = require("aws-sdk");
AWS.config.update({ region: 'ca-central-1' });
const dynamodb = new AWS.DynamoDB({ apiVersion: '2012-08-10' })

function get(event) {
    // Get all rows from the table
    const tableName = event.queryStringParameters.table;
    const params = {
        TableName: tableName
    };
    return dynamodb.scan(params).promise().then((response) => {
        const body = {
            statusCode: '200',
            // unmarshall the response
            body: JSON.stringify(response.Items.map(item => AWS.DynamoDB.Converter.unmarshall(item))),
        }
        return body;
    } , (error) => {
        console.log(error)
        return error;
    });
}

exports.handler = async (event, context) => {
  // Switch on the method
    switch (event.requestContext.http.method) {
        case 'GET':
            return get(event);
        default:
            // return JSON.stringify({ statusCode: 400, body: 'Unsupported method' });
            // return the entire event object for debugging
            return JSON.stringify(event);
    }
};