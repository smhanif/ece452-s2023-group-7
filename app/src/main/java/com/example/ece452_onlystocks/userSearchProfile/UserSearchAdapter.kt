package com.example.ece452_onlystocks.news

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.example.ece452_onlystocks.R
import com.example.ece452_onlystocks.StockInfoFragment
import com.example.ece452_onlystocks.UserProfileFragment
import com.example.ece452_onlystocks.constants.COMMON
import com.example.ece452_onlystocks.constants.DB_CONSTANTS
import com.example.ece452_onlystocks.userSearchProfile.UserSearchItem
import com.example.ece452_onlystocks.util.APIRequest
import com.example.ece452_onlystocks.util.getFundamentalData
import org.json.JSONObject


// reference for implementing RecyclerView adapter
// https://www.geeksforgeeks.org/how-to-implement-recylerview-in-a-fragment-in-android/

class UserSearchAdapter(private var userProfileRows: ArrayList<UserSearchItem>,  private val parentFragment: Fragment) : RecyclerView.Adapter<UserSearchAdapter.MyViewHolder>() {
    // This method creates a new ViewHolder object for each item in the RecyclerView
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        // Inflate the layout for each item and return a new ViewHolder object
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.profile_list_view, parent, false)
        return MyViewHolder(itemView)
    }

    // This method returns the total
    // number of items in the data set
    override fun getItemCount(): Int {
        return userProfileRows.size
    }

    // This method binds the data to the ViewHolder object for each item in the RecyclerView
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentRow = userProfileRows[position]

        holder.userName.text = currentRow.userName
        holder.name.text = "$" + currentRow.name
        val UserProfile = UserProfileFragment()
        val bundle = Bundle()
        bundle.putString("user", currentRow.userName)
        UserProfile.arguments = bundle
        holder.itemView.setOnClickListener { v ->
            val activity = v!!.context as AppCompatActivity
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(parentFragment.id, UserProfile)
                ?.addToBackStack(null)
                ?.commit()
        }


    }

    fun filter(newList: ArrayList<UserSearchItem>) {
        userProfileRows = ArrayList()
        userProfileRows.addAll(newList)
        notifyDataSetChanged()    }



    // This class defines the ViewHolder object for each item in the RecyclerView
    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val userName: TextView = itemView.findViewById(R.id.usernameField)
        val name: TextView = itemView.findViewById(R.id.nameField)
    }


}