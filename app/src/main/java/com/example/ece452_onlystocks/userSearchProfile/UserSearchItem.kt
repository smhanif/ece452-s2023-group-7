package com.example.ece452_onlystocks.userSearchProfile

data class UserSearchItem (
    var userName: String,
    var name: String
)