package com.example.ece452_onlystocks

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import at.favre.lib.crypto.bcrypt.BCrypt
import com.example.ece452_onlystocks.survey.SurveyInvestorLevel
import com.example.ece452_onlystocks.util.APIRequest
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import org.json.JSONObject


class SignInActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    private lateinit var googleSignInClient: GoogleSignInClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)

        auth = FirebaseAuth.getInstance()

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        googleSignInClient = GoogleSignIn.getClient(this, gso)

        val acct = GoogleSignIn.getLastSignedInAccount(this)
        if(acct != null){
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        findViewById<Button>(R.id.login).setOnClickListener {
            signInGoogle()

        }

        val username = findViewById<EditText>(R.id.username)
        val password = findViewById<EditText>(R.id.password)

        val loginBtn = findViewById<Button>(R.id.login)
        val signUpBtn = findViewById<Button>(R.id.signup)

        loginBtn.setOnClickListener {
            val usernameStr = username.text.toString().trim().lowercase()
            val passwordStr = password.text.toString()

            if (usernameStr.isEmpty()) {
                return@setOnClickListener
            }

            val tableName = "user"
            val primaryKey = "userName"
            val primaryKeyValue = usernameStr
            val endpoint = "https://pka2s6gf57.execute-api.ca-central-1.amazonaws.com/v1/test?table=$tableName&$primaryKey=$primaryKeyValue"

            val response = APIRequest(endpoint, "", "GET")
            println(response)

            if (response == "{}") {
                Toast.makeText(this, "Login Failed", Toast.LENGTH_SHORT).show()
            } else {
                val responseJSON = JSONObject(response)

                val passwordHash = responseJSON.get("Password").toString()
                val salt = responseJSON.get("PasswordSalt").toString()

                val result = BCrypt.verifyer().verify(passwordStr.plus(salt).toCharArray(), passwordHash.toCharArray())

                if (result.verified == true) {
                    val sharedPref = applicationContext.getSharedPreferences("MyPref", Context.MODE_PRIVATE)
                    with (sharedPref.edit()) {
                        putString("username", usernameStr)
                        apply()
                    }

                    val userName = sharedPref.getString("username", null)
                    println("Username should print here")
                    println(userName)

                    Toast.makeText(this, "Login Successful", Toast.LENGTH_SHORT).show()
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                } else {
                    Toast.makeText(this, "Login Failed", Toast.LENGTH_SHORT).show()
                }
            }
        }

        signUpBtn.setOnClickListener {
            val intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)
        }


    }

    private fun signInGoogle() {
        val signInIntent = googleSignInClient.signInIntent
        launcher.launch(signInIntent)

    }

    private val launcher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {

                val task = GoogleSignIn.getSignedInAccountFromIntent(result.data)
                handleResults(task)
            }
        }

    private fun handleResults(task: Task<GoogleSignInAccount>) {
        if (task.isSuccessful) {
            val account: GoogleSignInAccount? = task.result
            if (account != null) {
                updateUI(account)
            }
        } else {
            Toast.makeText(this, task.exception.toString(), Toast.LENGTH_SHORT).show()
        }
    }

    private fun updateUI(account: GoogleSignInAccount) {
        val credential = GoogleAuthProvider.getCredential(account.idToken, null)
        auth.signInWithCredential(credential).addOnCompleteListener {
            if (it.isSuccessful) {
                val isNew = it.getResult().additionalUserInfo?.isNewUser()
                if(isNew == true){
                    val intent: Intent = Intent(this, SurveyInvestorLevel::class.java)
                    intent.putExtra("email", account.email)
                    intent.putExtra("name", account.displayName)
                    startActivity(intent)
                }
                else{
                    val intent: Intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                }

            } else {
                Toast.makeText(this, it.exception.toString(), Toast.LENGTH_SHORT).show()

            }
        }
    }
}

