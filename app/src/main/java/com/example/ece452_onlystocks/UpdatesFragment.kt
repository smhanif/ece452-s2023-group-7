package com.example.ece452_onlystocks

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ece452_onlystocks.news.UpdatesAdapter
import com.example.ece452_onlystocks.updates.UpdatesFromFollowing
import com.example.ece452_onlystocks.util.getActionByUser
import com.example.ece452_onlystocks.util.getUser
import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVParser
import org.json.JSONArray
import java.io.BufferedReader
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale


class UpdatesFragment : Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }
    private fun readCSVFile(): CSVParser {
        var bufferReader = BufferedReader(context?.assets?.open("stock_universe.csv")?.reader())
        return CSVParser(bufferReader, CSVFormat.DEFAULT.withIgnoreHeaderCase(true))
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_updates, container, false)
    }
    private fun toDate(get: String): Date? {
        return SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(get)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val sharedPref = activity?.applicationContext?.getSharedPreferences("MyPref", Context.MODE_PRIVATE)
        val userName = sharedPref?.getString("username", null)
        val userObj = getUser(userName)

        //get current users following
        val following = userObj.get("following") as JSONArray
        val list = ArrayList<String>()
        for (i in 0 until following.length()) {
            list.add(following.getString(i))
        }


        //get all actions of following
        var actions = getActionByUser(list)

        val updatesList=ArrayList<UpdatesFromFollowing>()

        for (action in actions.sortedByDescending{ toDate(it.get("date") as String)}) {
            val userName = action.get("userName") as String
            val fname = action.get("FirstName") as String
            val lname = action.get("LastName") as String
            val ticker = action.get("ticker") as String
            val date = action.get("date") as String
            val targetPrice = action.get("targetPrice") as Number
            val act = if (action.get("add") as Boolean) 1 else 0
            val buy = if (action.get("buy") as Boolean) 1 else 0
            val companyName = readCSVFile().find { it.get(0) == ticker }?.get(1) as String

            updatesList.add(UpdatesFromFollowing(
                userName,
                fname,
                lname,
                R.drawable.profile_light_skin_god,
                date,
                ticker,
                companyName,
                act,
                buy,
                targetPrice
            ))
        }

        // Assign updatesList to UpdatesAdapter
        val itemAdapter= UpdatesAdapter(updatesList)

        // Set the LayoutManager that this RecyclerView will use.
        val recyclerView:RecyclerView=view.findViewById(R.id.updates_recyclerview)
        recyclerView.layoutManager = LinearLayoutManager(context)

        // adapter instance is set to the recyclerview to inflate the items.
        recyclerView.adapter = itemAdapter
    }
}