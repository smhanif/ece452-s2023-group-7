package com.example.ece452_onlystocks

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.ListView
import android.widget.RadioButton
import android.widget.TextView
import androidx.compose.ui.text.toLowerCase
import android.widget.SearchView
import com.example.ece452_onlystocks.constants.COMMON.STOCKS
import androidx.fragment.app.DialogFragment
import com.example.ece452_onlystocks.util.addActionToDB
import com.example.ece452_onlystocks.util.addStockToWatchlist
import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVParser
import java.io.BufferedReader
import android.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class AddStock : DialogFragment() {
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var listView: ListView
    private lateinit var inputSearch: TextView
    private lateinit var submit: Button
    private lateinit var stockName: TextView
    private lateinit var thesis: EditText
    private lateinit var target_price: EditText
    private lateinit var ticker: String
    private lateinit var radioButton1: RadioButton
    private lateinit var radioButton2: RadioButton


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_add_stock, container, false)
        listView = view.findViewById(R.id.simpleListView)
        inputSearch = view.findViewById(R.id.searchView)
        submit = view.findViewById(R.id.submitStock)
        stockName = view.findViewById(R.id.stockName)
        thesis = view.findViewById(R.id.thesis)
        target_price = view.findViewById(R.id.target_price)
        radioButton1 = view.findViewById(R.id.radioButton1)
        radioButton2 = view.findViewById(R.id.radioButton2)


        stockName.visibility = View.GONE;

        val sharedPref = activity?.applicationContext?.getSharedPreferences("MyPref", Context.MODE_PRIVATE)
        val userName = sharedPref?.getString("username", null)

        var list = mutableListOf<String>()
        for (csvRecord in readCSVFile()) {
            list.add(csvRecord.get(0) + " - " + csvRecord.get(1))
        }
        list.removeAt(0)
        val adapter =
            context?.let { ArrayAdapter(it, android.R.layout.simple_list_item_1, list) }
        listView.adapter = adapter

        inputSearch.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
                //Change the visibility here
                listView.visibility = View.VISIBLE;
                stockName.visibility = View.GONE;
                adapter?.filter?.filter(s)

            }
        })

        listView.setOnItemClickListener { _, _, position, _ ->
            val selectedFromList = listView.getItemAtPosition(position)
            inputSearch.setText(selectedFromList.toString())
            listView.visibility = View.GONE;
            stockName.text = selectedFromList.toString()
            stockName.text = "$" + selectedFromList.toString().substringBefore("-").trim()
            stockName.visibility = View.VISIBLE;
            ticker = selectedFromList.toString().substringBefore("-").trim()
            Log.d("CSV_symbol", selectedFromList.toString())
        }
        submit.setOnClickListener {

            if(inputSearch.text.toString().trim().isEmpty()){
                inputSearch.error = "Please add a stock"
                return@setOnClickListener
            }
            else if(!radioButton1.isChecked && !radioButton2.isChecked){
                radioButton1.error = "Please select an option"
                return@setOnClickListener
            }
            else if(target_price.text.toString().trim().isEmpty()){
                target_price.error = "Please input a target price"
                return@setOnClickListener
            }
            else if(thesis.text.toString().trim().isEmpty()){
                thesis.error = "Please input a thesis"
                return@setOnClickListener
            }
            else {
                addStockToWatchlist(userName, ticker, target_price, radioButton1, thesis)
                addActionToDB(userName, ticker, target_price, radioButton1)
                parentFragmentManager.setFragmentResult("TRIGGER_REFRESH", Bundle());
                dismiss()
            }
        }
        return view
    }

    private fun readCSVFile(): CSVParser {
        var bufferReader = BufferedReader(context?.assets?.open(STOCKS)?.reader())
        return CSVParser(bufferReader, CSVFormat.DEFAULT.withIgnoreHeaderCase(true))
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            AddStock().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}