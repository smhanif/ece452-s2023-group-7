package com.example.ece452_onlystocks.constants

object DB_CONSTANTS {
    const val WATCHLIST_TABLE = "watchlistItem"
    const val ACTIONS_TABLE = "actions"
    const val STOCK_TABLE = "stock"
    const val USER_TABLE = "user"
    const val BASE_URL = "https://pka2s6gf57.execute-api.ca-central-1.amazonaws.com/v1/test"
}