package com.example.ece452_onlystocks.constants

object COMMON {
    const val GET = "GET"
    const val POST = "POST"
    const val PUT = "PUT"
    const val DELETE = "DELETE"
    const val STOCKS = "stock_universe.csv"
}