package com.example.ece452_onlystocks

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.ece452_onlystocks.constants.COMMON
import com.example.ece452_onlystocks.util.APIRequest

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class StockInfoFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val sharedPref =
            activity?.applicationContext?.getSharedPreferences("MyPref", Context.MODE_PRIVATE)
        val userName = sharedPref?.getString("username", null)
        val view = inflater.inflate(R.layout.fragment_stock_info, container, false)
        val HomeFragment = HomeFragment()
        view.findViewById<Button>(R.id.remove_from_watchlist_button).setOnClickListener{
            APIRequest("https://pka2s6gf57.execute-api.ca-central-1.amazonaws.com/v1/test?table=watchlistItem&userName=${userName}&ticker=${this.arguments?.getString("ticker", "ticker")}", "", COMMON.DELETE)
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(((view as ViewGroup).parent as View).id, HomeFragment)
                ?.addToBackStack(null)
                ?.commit()
        }
        val bundle = this.arguments
        if (bundle != null) {
            view.findViewById<TextView>(R.id.ticker).text = bundle.getString("ticker", "ticker")
            view.findViewById<TextView>(R.id.market_cap).text = bundle.getDouble("MktCap", 0.0).toString()
            view.findViewById<TextView>(R.id.avg_volume).text = bundle.getDouble("avgVolume", 0.0).toString()
            view.findViewById<TextView>(R.id.PETTM).text = bundle.getDouble("PE", 0.0).toString()
            view.findViewById<TextView>(R.id.EPSTTM).text = bundle.getDouble("EPS", 0.0).toString()
            view.findViewById<TextView>(R.id.high_52_weeks).text = bundle.getDouble("wkHigh", 0.0).toString()
            view.findViewById<TextView>(R.id.low_52_weeks).text = bundle.getDouble("wkLow", 0.0).toString()
            view.findViewById<TextView>(R.id.target_price).text = bundle.getDouble("targetPrice", 0.0).toString()
            view.findViewById<TextView>(R.id.thesis).text = bundle.getString("thesis", "thesis")
        }
        inflater.inflate(R.layout.fragment_stock_info, container, false)

        // go back button
        view.findViewById<ImageButton>(R.id.go_back).setOnClickListener{
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(((view as ViewGroup).parent as View).id, HomeFragment)
                ?.addToBackStack(null)
                ?.commit()
        }

        return view
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            StockInfoFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}