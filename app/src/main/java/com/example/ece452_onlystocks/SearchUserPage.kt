package com.example.ece452_onlystocks

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ece452_onlystocks.news.UserSearchAdapter
import com.example.ece452_onlystocks.userSearchProfile.UserSearchItem
import com.example.ece452_onlystocks.util.getUsers


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class SearchUserPage : Fragment() {
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var userSearch: SearchView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_search_user_page, container, false)
        userSearch = view.findViewById(R.id.userSearch)

        val users = getUsers()

        val userSearchRows=ArrayList<UserSearchItem>()

        // Get your own username so that we can exclude it from search
        val sharedPref = activity?.applicationContext?.getSharedPreferences("MyPref", Context.MODE_PRIVATE)
        val myUserName = sharedPref?.getString("username", null)

        for (i in 0 until users.length()) {
            val user = users.getJSONObject(i)
            if (user.get("userName") as String != myUserName) {
                userSearchRows.add(UserSearchItem(user.get("userName") as String, user.get("FirstName") as String))
            }
        }

        // Assign updatesList to WatchlistAdapter
        val itemAdapter= UserSearchAdapter(userSearchRows, this)

        // Set the LayoutManager that this RecyclerView will use.
        val recyclerView: RecyclerView =view.findViewById(R.id.userSearch_recyclerview)
//        recyclerView.setVisibility(View.GONE);
        recyclerView.layoutManager = LinearLayoutManager(context)

        // adapter instance is set to the recyclerview to inflate the items.
        recyclerView.adapter = itemAdapter

        userSearch.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                val input = newText.lowercase()
                val newList: ArrayList<UserSearchItem> = ArrayList()
                for (user in userSearchRows) {
                    val userName: String = user.userName.lowercase()
                    val name: String = user.name.lowercase()

                    if (userName.contains(input) || name.contains(input)) {
                        newList.add(user)
                    }
                }
                itemAdapter.filter(newList)
                return true
            }
        })

        return view
    }

        companion object {
            @JvmStatic
            fun newInstance(param1: String, param2: String) =
                SearchUserPage().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
        }
    }