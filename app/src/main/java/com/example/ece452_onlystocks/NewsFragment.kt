package com.example.ece452_onlystocks

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ece452_onlystocks.constants.API_CONSTANTS
import com.example.ece452_onlystocks.constants.COMMON
import com.example.ece452_onlystocks.constants.DB_CONSTANTS
import com.example.ece452_onlystocks.data.NewsData
import com.example.ece452_onlystocks.data.NewsInterface
import com.example.ece452_onlystocks.news.NewsAdapter
import com.example.ece452_onlystocks.news.NewsArticle
import com.example.ece452_onlystocks.util.APIRequest
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class NewsFragment : Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_news, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //build retrofit request
        val retrofitBuilder = Retrofit.Builder()
            .baseUrl(API_CONSTANTS.MARKET_NEWS_API_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(NewsInterface::class.java)

        val sharedPref = activity?.applicationContext?.getSharedPreferences("MyPref", Context.MODE_PRIVATE)
        val userName = sharedPref?.getString("username", null)
        //get watchlist items for current user
        val table = DB_CONSTANTS.WATCHLIST_TABLE
        val baseURL = DB_CONSTANTS.BASE_URL
        var endpoint = "$baseURL?table=$table&userName=$userName"
        val resp = APIRequest(endpoint, "", COMMON.GET)

        val respJSON = JSONArray(resp)

        val tickers = StringBuilder()

        for (i in 0 until respJSON.length()) {
            val item: JSONObject = respJSON.getJSONObject(i)
            tickers.append(item.get("ticker"))
            if (i != respJSON.length()-1){
                tickers.append(",")
            }
        }

        println(tickers)
        val retrofitData =
            retrofitBuilder.getNews(API_CONSTANTS.MARKET_NEWS_API_TOKEN, tickers.toString(), API_CONSTANTS.ENTITY)

        retrofitData.enqueue(object : Callback<NewsData?> {
            override fun onResponse(call: Call<NewsData?>, response: Response<NewsData?>) {
                val responseBody = response.body()?.data!!
                val newsList = ArrayList<NewsArticle>()

                for (news in responseBody) {
                    newsList.add(
                        NewsArticle(
                            news.published_at.substringBefore("T"),
                            news.title,
                            news.snippet,
                            news.url
                        )
                    )
                }

                // Assign updatesList to NewsAdapter
                val itemAdapter = NewsAdapter(newsList)

                // Set the LayoutManager that this RecyclerView will use.
                val recyclerView: RecyclerView = view.findViewById(R.id.news_recyclerview)
                recyclerView.layoutManager = LinearLayoutManager(context)

                // adapter instance is set to the recyclerview to inflate the items.
                recyclerView.adapter = itemAdapter

            }

            override fun onFailure(call: Call<NewsData?>, t: Throwable) {
            }
        })
    }
}