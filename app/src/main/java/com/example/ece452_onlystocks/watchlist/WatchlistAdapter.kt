package com.example.ece452_onlystocks.news

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.example.ece452_onlystocks.R
import com.example.ece452_onlystocks.StockInfoFragment
import com.example.ece452_onlystocks.constants.API_CONSTANTS
import com.example.ece452_onlystocks.constants.COMMON
import com.example.ece452_onlystocks.constants.DB_CONSTANTS
import com.example.ece452_onlystocks.util.APIRequest
import com.example.ece452_onlystocks.util.getFundamentalData
import com.example.ece452_onlystocks.watchlist.WatchlistItem
import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVParser
import org.json.JSONObject
import java.io.BufferedReader
import java.math.BigDecimal

// reference for implementing RecyclerView adapter
// https://www.geeksforgeeks.org/how-to-implement-recylerview-in-a-fragment-in-android/

class WatchlistAdapter(private val watchlistRows: ArrayList<WatchlistItem>, private val parentFragment: Fragment) : RecyclerView.Adapter<WatchlistAdapter.MyViewHolder>() {
    // This method creates a new ViewHolder object for each item in the RecyclerView
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        // Inflate the layout for each item and return a new ViewHolder object
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.watchlist_row, parent, false)
        return MyViewHolder(itemView)
    }

    // This method returns the total
    // number of items in the data set
    override fun getItemCount(): Int {
        return watchlistRows.size
    }

    // This method binds the data to the ViewHolder object for each item in the RecyclerView
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentRow = watchlistRows[position]

        holder.ticker.text = currentRow.ticker
        holder.current_price.text = "$" + currentRow.current_price.toString()

        holder.pl_percentage_day.text = currentRow.pl_percentage_day.toString() + "%"
        holder.pl_percentage_day.setTextColor(Color.rgb(0, 0, 0))
        if (currentRow.pl_percentage_day > BigDecimal(0)) {
            holder.pl_percentage_day.setTextColor(Color.rgb(19, 150, 2))
        } else if (currentRow.pl_percentage_day < BigDecimal(0)){
            holder.pl_percentage_day.setTextColor(Color.rgb(189, 48, 48))
        }

        if (currentRow.buy_or_sell) {
            holder.buy_or_sell.text = "Buy"
        } else {
            holder.buy_or_sell.text = "Sell"
        }

        holder.itemView.setOnClickListener { v ->
            val activity = v!!.context as AppCompatActivity
            val newFragment = StockInfoFragment()
            val bundle = Bundle()
            val json = getFundamentalData(currentRow.ticker)
            val sharedPref =
                activity?.applicationContext?.getSharedPreferences("MyPref", Context.MODE_PRIVATE)
            val userName = sharedPref?.getString("username", null)
            val jsonObj = JSONObject(
                APIRequest(
                    "${DB_CONSTANTS.BASE_URL}?table=${DB_CONSTANTS.WATCHLIST_TABLE}&userName=${userName}&ticker=${currentRow.ticker}",
                    "",
                    COMMON.GET
                )
            )
            bundle.putString("ticker", currentRow.ticker)
            bundle.putString("companyName", currentRow.ticker)
            bundle.putDouble("MktCap", (json.get("marketCapitalization") as Number).toDouble())
            bundle.putDouble(
                "avgVolume",
                (json.get("10DayAverageTradingVolume") as Number).toDouble()
            )
            bundle.putDouble("PE", (json.get("peTTM") as Number).toDouble())
            bundle.putDouble("EPS", (json.get("epsTTM") as Number).toDouble())
            bundle.putDouble("wkHigh", (json.get("52WeekHigh") as Number).toDouble())
            bundle.putDouble("wkLow", (json.get("52WeekLow") as Number).toDouble())
            bundle.putDouble("targetPrice", (jsonObj.get("targetPrice") as Number).toDouble())
            bundle.putString("thesis", jsonObj.get("thesis") as String)
            newFragment.arguments = bundle
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(parentFragment.id, newFragment)
                ?.addToBackStack(null)
                ?.commit()
        }


    }

    // This class defines the ViewHolder object for each item in the RecyclerView
    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val ticker: TextView = itemView.findViewById(R.id.ticker)
        val current_price: TextView = itemView.findViewById(R.id.current_price)
        val pl_percentage_day: TextView = itemView.findViewById(R.id.pl_percentage_day)
        val buy_or_sell: TextView = itemView.findViewById(R.id.buy_or_sell)
    }
}