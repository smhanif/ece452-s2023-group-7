package com.example.ece452_onlystocks.watchlist

import java.math.BigDecimal

data class WatchlistItem(
    var ticker: String,
    var current_price: BigDecimal,
    var pl_percentage_day: BigDecimal,
    var buy_or_sell: Boolean)