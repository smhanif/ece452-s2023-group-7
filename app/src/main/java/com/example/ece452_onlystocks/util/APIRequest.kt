package com.example.ece452_onlystocks.util
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody


fun sendRequest(url: String, body: String, method: String): String {
    val client = OkHttpClient()
    val postBody = body.toRequestBody("application/json".toMediaTypeOrNull())
    println("postBody = ${postBody.toString()}")
    val request = when (method) {
        "POST" -> Request.Builder()
            .url(url)
            .post(postBody)
            .build()
        "GET" -> Request.Builder()
            .url(url)
            .get()
            .build()
        "DELETE" -> Request.Builder()
            .url(url)
            .delete()
            .build()
        "PUT" -> Request.Builder()
            .url(url)
            .put(body.toRequestBody("application/json".toMediaTypeOrNull()))
            .build()
        else -> return "Invalid method"
    }

    client.newCall(request).execute().use { response ->
        if (!response.isSuccessful) return "Unexpected code $response"
        return response.body?.string().toString()
    }
}

fun APIRequest(url: String, body: String, method: String): String {
    // wrap sendRequest in another thread to avoid NetworkOnMainThreadException
    var response = ""
    val thread = Thread {
        response = sendRequest(url, body, method)
    }
    thread.start()
    thread.join()
    return response
}