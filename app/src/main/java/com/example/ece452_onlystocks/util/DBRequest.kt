package com.example.ece452_onlystocks.util

import android.widget.EditText
import android.widget.RadioButton
import com.example.ece452_onlystocks.constants.API_CONSTANTS
import com.example.ece452_onlystocks.constants.COMMON
import com.example.ece452_onlystocks.constants.DB_CONSTANTS
import org.json.JSONArray
import org.json.JSONObject
import java.time.LocalDate
import java.time.LocalDateTime

fun addActionToDB(
    userName: String?,
    ticker: String,
    target_price: EditText,
    radioButton1: RadioButton
) {

    val table = DB_CONSTANTS.ACTIONS_TABLE
    val baseURL = DB_CONSTANTS.BASE_URL
    var endpoint = "$baseURL?table=$table&userName=$userName"

    val requestBody = """
                {
                    "actionID": {"S": "${userName + "_" + LocalDateTime.now()}"},
                    "ticker": {"S": "$ticker"},
                    "userName": {"S": "$userName"},
                    "add": {"BOOL": ${true}},
                    "date": {"S": "${LocalDate.now()}"},
                    "targetPrice": {"N": "${target_price.text}"},
                    "buy": {"BOOL": ${radioButton1.isChecked}}
                }
                """.trimIndent()
    val resp = APIRequest(endpoint, requestBody, COMMON.POST)
    println(resp)
}

fun addStockToWatchlist(
    userName: String?,
    ticker: String,
    target_price: EditText,
    radioButton1: RadioButton,
    thesis: EditText
) {
    //add stock to watchlist
    val table = DB_CONSTANTS.WATCHLIST_TABLE
    val baseURL = DB_CONSTANTS.BASE_URL
    var endpoint = "$baseURL?table=$table&userName=$userName"

    val requestBody = """
                {
                    "userName": {"S": "$userName"},
                    "ticker": {"S": "$ticker"},
                    "buy": {"BOOL": ${radioButton1.isChecked}},
                    "targetPrice": {"N": "${target_price.text}"},
                    "thesis": {"S": "${thesis.text}"},
                    "date": {"S": "${LocalDate.now()}"}
                }
                """.trimIndent()
    APIRequest(endpoint, requestBody, COMMON.POST)
}

fun getUsers(): JSONArray {
    var endpoint =
        "https://pka2s6gf57.execute-api.ca-central-1.amazonaws.com/v1/all?table=${DB_CONSTANTS.USER_TABLE}"
    return JSONArray(APIRequest(endpoint, "", COMMON.GET))
}

fun getUser(userName: String?): JSONObject {
    val table = DB_CONSTANTS.USER_TABLE
    var endpoint = "${DB_CONSTANTS.BASE_URL}?table=$table&userName=$userName"
    return JSONObject(APIRequest(endpoint, "", COMMON.GET))
}

fun getActionByUser(following: ArrayList<String>): ArrayList<JSONObject> {
    val table = DB_CONSTANTS.ACTIONS_TABLE
    val actions = ArrayList<JSONObject>();
    for (user in following) {
        var endpoint = "${DB_CONSTANTS.BASE_URL}?table=$table&userName=$user"
        val resp = APIRequest(endpoint, "", COMMON.GET)
        val response = getUser(user)
        val userActions = JSONArray(resp)
        for (i in 0 until userActions.length()) {
            val action: JSONObject = userActions.getJSONObject(i)
            action.put("FirstName", response.get("FirstName"))
            action.put("LastName", response.get("LastName"))
            actions.add(action)
        }
    }
    return actions
}

fun getFundamentalData(ticker: String): JSONObject {
    var endpoint = "${API_CONSTANTS.FUNDAMENTAL_DATA_API_URL}?symbol=$ticker&metric=all&token=${API_CONSTANTS.FUNDAMENTAL_DATA_API_TOKEN}"
    return JSONObject(APIRequest(endpoint, "", COMMON.GET)).get("metric") as JSONObject
}

fun updateFollowerFollowing(follower: String?, followed: String?, add: Boolean){
    updateFollower(follower,followed, add)
    updateFollowing(follower,followed, add)
}

fun updateFollower(follower: String?, followed: String?, add: Boolean) {
    //Update the follower's following list with the new user
    val table = DB_CONSTANTS.USER_TABLE
    val baseURL = DB_CONSTANTS.BASE_URL

    val followerObj = JSONObject(APIRequest("$baseURL?table=$table&userName=$follower", "", COMMON.GET))
    println(followerObj)
    val current_following = followerObj.get("following") as JSONArray

    if(add) {
        current_following.put(followed)
    }else
    {
        for (i in 0 until current_following.length()) {
            val following: String = current_following.getString(i)
            if(following.equals(followed))
            {
                current_following.remove(i)
                break
            }
        }
    }
    val updatedFollowing = ArrayList<JSONObject>()

    for (i in 0 until current_following.length()) {
        val following: String = current_following.getString(i)
        updatedFollowing.add(JSONObject("""{"S": ${following}}"""))
    }

    val current_followers = followerObj.get("followers") as JSONArray

    val updatedFollowers = ArrayList<JSONObject>()

    for (i in 0 until current_followers.length()) {
        val follower: String = current_followers.getString(i)
        updatedFollowers.add(JSONObject("""{"S": ${follower}}"""))
    }

    val followerUserSchema  =
        """
                {
                  "userName": {"S": "${followerObj.get("userName")}"},
                  "Password": {"S": "${followerObj.get("Password")}"},
                  "PasswordSalt": {"S": "${followerObj.get("PasswordSalt")}"},
                  "FirstName": {"S": "${followerObj.get("FirstName")}"},
                  "LastName": {"S": "${followerObj.get("LastName")}"},
                  "email": {"S": "${followerObj.get("email")}"},
                  "redditUserName": {"S": "${followerObj.get("redditUserName")}"},
                  "twitterUserName": {"S": "${followerObj.get("twitterUserName")}"},
                  "followers": {"L": ${updatedFollowers}},
                  "following": {"L": ${updatedFollowing}},
                  "bio": {"S": "${followerObj.get("bio")}"}
                }
    """.trimIndent()

    println(APIRequest("https://pka2s6gf57.execute-api.ca-central-1.amazonaws.com/v1/test?table=user", followerUserSchema, COMMON.PUT))
}


fun updateFollowing(follower: String?, followed: String?, add: Boolean){
    //Update the followed users followed list
    val table = DB_CONSTANTS.USER_TABLE
    val baseURL = DB_CONSTANTS.BASE_URL

    val followedObj = JSONObject(APIRequest("$baseURL?table=$table&userName=$followed", "", COMMON.GET))

    val current_followers = followedObj.get("followers") as JSONArray

    if(add) {
        current_followers.put(follower)
    }else{
        for (i in 0 until current_followers.length()) {
            val following: String = current_followers.getString(i)
            if(following==follower){
                current_followers.remove(i)
                break
            }
        }
    }

    val updatedFollowers = ArrayList<JSONObject>()

    for (i in 0 until current_followers.length()) {
        val following: String = current_followers.getString(i)
        updatedFollowers.add(JSONObject("""{"S": ${following}}"""))
    }

    val current_following = followedObj.get("following") as JSONArray

    val updatedFollowing = ArrayList<JSONObject>()

    for (i in 0 until current_following.length()) {
        val following: String = current_following.getString(i)
        updatedFollowing.add(JSONObject("""{"S": ${following}}"""))
    }

    val followerUserSchema  =
        """
                {
                  "userName": {"S": "${followedObj.get("userName")}"},
                  "Password": {"S": "${followedObj.get("Password")}"},
                  "PasswordSalt": {"S": "${followedObj.get("PasswordSalt")}"},
                  "FirstName": {"S": "${followedObj.get("FirstName")}"},
                  "LastName": {"S": "${followedObj.get("LastName")}"},
                  "email": {"S": "${followedObj.get("email")}"},
                  "redditUserName": {"S": "${followedObj.get("redditUserName")}"},
                  "twitterUserName": {"S": "${followedObj.get("twitterUserName")}"},
                  "followers": {"L": ${updatedFollowers}},
                  "following": {"L": ${updatedFollowing}},
                  "bio": {"S": "${followedObj.get("bio")}"}
                }
    """.trimIndent()

    println(APIRequest("https://pka2s6gf57.execute-api.ca-central-1.amazonaws.com/v1/test?table=user", followerUserSchema, COMMON.PUT))

}

fun isFollowing(user1: String?, user2: String?): Boolean {
    //Update the followed users followed list
    val table = DB_CONSTANTS.USER_TABLE
    val baseURL = DB_CONSTANTS.BASE_URL
    val followedObj = JSONObject(APIRequest("$baseURL?table=$table&userName=$user1", "", COMMON.GET))

    val current_following = followedObj.get("following") as JSONArray

    for (i in 0 until current_following.length()) {
        val following: String = current_following.getString(i)
        if(following==user2){
            return true
        }
    }
    return false
}

fun getCommonFollowers(user1: String?, user2: String?): Int {
    //Update the followed users followed list
    val table = DB_CONSTANTS.USER_TABLE
    val baseURL = DB_CONSTANTS.BASE_URL
    val user1Obj = JSONObject(APIRequest("$baseURL?table=$table&userName=$user1", "", COMMON.GET))
    val user2Obj = JSONObject(APIRequest("$baseURL?table=$table&userName=$user2", "", COMMON.GET))

    val user1JArray = user1Obj.get("followers") as JSONArray
    val user1Followers = ArrayList<String>()
    for (i in 0 until user1JArray.length()) {
        user1Followers.add(user1JArray.getString(i))
    }

    val user2JArray = user2Obj.get("followers") as JSONArray
    val user2Followers = ArrayList<String>()
    for (i in 0 until user2JArray.length()) {
        user2Followers.add(user2JArray.getString(i))
    }

    val commonFollowers = user1Followers.intersect(user2Followers.toSet())

    return commonFollowers.size
}

fun getCommonStocks(user1: String?, user2: String?) : Int {
    //Update the followed users followed list
    val table = DB_CONSTANTS.WATCHLIST_TABLE
    val baseURL = DB_CONSTANTS.BASE_URL
    val user1Objs = APIRequest("$baseURL?table=$table&userName=$user1", "", COMMON.GET)
    val user2Objs = APIRequest("$baseURL?table=$table&userName=$user2", "", COMMON.GET)

    val JSONString = "{\"user1\": $user1Objs, \"user2\": $user2Objs}"
    val obj = JSONObject(JSONString)

    val user1JArray = obj.getJSONArray("user1")
    val user1Stocks = ArrayList<String>()
    for (i in 0 until user1JArray.length()) {
        user1Stocks.add(user1JArray.getJSONObject(i).getString("ticker"))
    }

    val user2JArray = obj.getJSONArray("user2")
    val user2Stocks = ArrayList<String>()
    for (i in 0 until user2JArray.length()) {
        user2Stocks.add(user2JArray.getJSONObject(i).getString("ticker"))
    }

    val commonStocks = user1Stocks.intersect(user2Stocks.toSet())

    return commonStocks.size
}