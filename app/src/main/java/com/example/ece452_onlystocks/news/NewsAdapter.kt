package com.example.ece452_onlystocks.news

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.ece452_onlystocks.R

// reference for implementing RecyclerView adapter
// https://www.geeksforgeeks.org/how-to-implement-recylerview-in-a-fragment-in-android/

class NewsAdapter(private val articles: ArrayList<NewsArticle>) : RecyclerView.Adapter<NewsAdapter.MyViewHolder>() {
    // This method creates a new ViewHolder object for each item in the RecyclerView
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        // Inflate the layout for each item and return a new ViewHolder object
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.news_article, parent, false)
        return MyViewHolder(itemView)
    }

    // This method returns the total
    // number of items in the data set
    override fun getItemCount(): Int {
        return articles.size
    }

    // This method binds the data to the ViewHolder object for each item in the RecyclerView
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentUpdate = articles[position]

        holder.time.text = currentUpdate.time
        holder.title.text = currentUpdate.title
        holder.snippet.text = currentUpdate.snippet
        holder.url.text = currentUpdate.url
    }

    // This class defines the ViewHolder object for each item in the RecyclerView
    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val time: TextView = itemView.findViewById(R.id.article_time)
        val title: TextView = itemView.findViewById(R.id.article_title)
        val snippet: TextView = itemView.findViewById(R.id.article_snippet)
        val url: TextView = itemView.findViewById(R.id.article_url)
    }
}