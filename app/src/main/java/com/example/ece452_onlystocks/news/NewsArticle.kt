package com.example.ece452_onlystocks.news

import android.media.Image

data class NewsArticle(
    var time: String,
    var title: String,
    var snippet: String,
    var url: String)