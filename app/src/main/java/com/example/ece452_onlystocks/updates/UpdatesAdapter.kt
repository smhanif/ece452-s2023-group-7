package com.example.ece452_onlystocks.news

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.ece452_onlystocks.R
import com.example.ece452_onlystocks.updates.UpdatesFromFollowing

// reference for implementing RecyclerView adapter
// https://www.geeksforgeeks.org/how-to-implement-recylerview-in-a-fragment-in-android/

class UpdatesAdapter(private val updates: ArrayList<UpdatesFromFollowing>) : RecyclerView.Adapter<UpdatesAdapter.MyViewHolder>() {
    // This method creates a new ViewHolder object for each item in the RecyclerView
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        // Inflate the layout for each item and return a new ViewHolder object
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.updates_from_following, parent, false)
        return MyViewHolder(itemView)
    }

    // This method returns the total
    // number of items in the data set
    override fun getItemCount(): Int {
        return updates.size
    }

    // This method binds the data to the ViewHolder object for each item in the RecyclerView
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentUpdate = updates[position]

        holder.profile_pic.setImageResource(currentUpdate.userProfilePic)

        if (currentUpdate.action.toString() == "1") {
            holder.action_icon.setImageResource(R.drawable.icon_add)
            holder.action.text = "Added to their watchlist"
        } else {
            holder.action_icon.setImageResource(R.drawable.icon_remove)
            holder.action.text = "Removed from their watchlist"
            holder.buy_or_sell_and_target_price.text = currentUpdate.userName + " · " + currentUpdate.time
        }


        holder.name_and_time.text = currentUpdate.firstName + " " + currentUpdate.lastName + " · " + currentUpdate.time
        holder.ticker_and_company.text = currentUpdate.companyName + " (" + currentUpdate.ticker + ")"

        if (currentUpdate.buy.toString() == "1") {
            holder.buy_or_sell_and_target_price.text = "Buy · $" + currentUpdate.targetPrice.toString()
        } else {
            holder.buy_or_sell_and_target_price.text = "Sell · $" + currentUpdate.targetPrice.toString()
        }
    }

    // This class defines the ViewHolder object for each item in the RecyclerView
    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val profile_pic: ImageView = itemView.findViewById(R.id.profile_pic)
        val action_icon: ImageView = itemView.findViewById(R.id.action_icon)
        val action: TextView = itemView.findViewById(R.id.action)
        val name_and_time: TextView = itemView.findViewById(R.id.name_and_time)
        val ticker_and_company: TextView = itemView.findViewById(R.id.ticker_and_company)
        val buy_or_sell_and_target_price: TextView = itemView.findViewById(R.id.buy_or_sell_and_target_price)
    }
}