package com.example.ece452_onlystocks.updates

data class UpdatesFromFollowing(
    var userName: String,
    var firstName: String,
    var lastName: String,
    var userProfilePic: Int,
    var time: String,
    var ticker: String,
    var companyName: String,
    var action: Int,
    var buy: Int,
    var targetPrice: Number)