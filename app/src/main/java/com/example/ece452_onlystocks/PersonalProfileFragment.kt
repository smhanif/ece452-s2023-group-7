package com.example.ece452_onlystocks

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import com.example.ece452_onlystocks.util.getUser
import org.json.JSONArray

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class PersonalProfileFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_personal_profile, container, false)
        val sharedPref = activity?.applicationContext?.getSharedPreferences("MyPref", Context.MODE_PRIVATE)
        val userName = sharedPref?.getString("username", null)
        val userObj = getUser(userName)
        val fullName = userObj.get("FirstName") as String + " " + userObj.get("LastName") as String
        view.findViewById<TextView>(R.id.profile_name).text = fullName
        view.findViewById<TextView>(R.id.followers_count).text = (userObj.get("followers") as JSONArray).length().toString()
        view.findViewById<TextView>(R.id.following_count).text = (userObj.get("following") as JSONArray).length().toString()

        // bio
        val bio = userObj.get("bio") as? String
        view.findViewById<TextView>(R.id.profile_bio).text = bio

        // twitter link
        val twitterUserName = userObj.get("twitterUserName") as? String
        val twitterLink = "https://www.twitter.com/$twitterUserName"
        val twitterButton: ImageButton = view.findViewById(R.id.twitter_profile_link)
        twitterButton.setOnClickListener {
            goToLink(twitterLink)
        }

        // reddit link
        val redditUserName = userObj.get("redditUserName") as? String
        val redditLink = "https://www.reddit.com/user/$redditUserName"
        val redditButton: ImageButton = view.findViewById(R.id.reddit_profile_link)
        redditButton.setOnClickListener {
            goToLink(redditLink)
        }

        return view
    }

    private fun goToLink(url: String) {
        val uriUrl = Uri.parse(url)
        val launchBrowser = Intent(Intent.ACTION_VIEW, uriUrl)
        startActivity(launchBrowser)
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            PersonalProfileFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}