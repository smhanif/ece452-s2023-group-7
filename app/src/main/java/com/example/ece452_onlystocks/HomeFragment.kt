package com.example.ece452_onlystocks

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ece452_onlystocks.constants.API_CONSTANTS
import com.example.ece452_onlystocks.constants.COMMON
import com.example.ece452_onlystocks.constants.DB_CONSTANTS
import com.example.ece452_onlystocks.news.WatchlistAdapter
import com.example.ece452_onlystocks.util.APIRequest
import com.example.ece452_onlystocks.watchlist.WatchlistItem
import org.json.JSONArray
import org.json.JSONObject
import java.math.BigDecimal
import java.math.RoundingMode


class HomeFragment: Fragment()  {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        parentFragmentManager.setFragmentResultListener(
            "TRIGGER_REFRESH", this
        ) { requestKey: String, _: Bundle? ->
            // Handle the result received from the modal fragment and trigger a refresh
            if (requestKey == "TRIGGER_REFRESH") {
                parentFragmentManager.beginTransaction().detach(this).commit ()
                parentFragmentManager.beginTransaction().attach(this).commit ()
            }
        }

        val button: Button = view.findViewById(R.id.add_stock_button)
        button.setOnClickListener{
            val showModal = AddStock()
            showModal.show((activity as AppCompatActivity).supportFragmentManager, "showPopUp")
        }

        val inputSearch: Button = view.findViewById(R.id.searchUsers)

        inputSearch.setOnClickListener{
            val userSearch = SearchUserPage()
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(((view as ViewGroup).parent as View).id, userSearch)
                ?.addToBackStack(null)
                ?.commit()
        }
        val sharedPref = activity?.applicationContext?.getSharedPreferences("MyPref", Context.MODE_PRIVATE)
        val userName = sharedPref?.getString("username", null)
        //get watchlist items for current user
        val table = DB_CONSTANTS.WATCHLIST_TABLE
        val baseURL = DB_CONSTANTS.BASE_URL
        var endpoint = "$baseURL?table=$table&userName=$userName"
        val resp = APIRequest(endpoint, "", COMMON.GET)

        val respJSON = JSONArray(resp)

        val watchlistRows=ArrayList<WatchlistItem>()

        for (i in 0 until respJSON.length()) {

            // store each object in JSONObject
            val item: JSONObject = respJSON.getJSONObject(i)
            val ticker = item.get("ticker")
            val buy = item.get("buy")
            val response = APIRequest("${API_CONSTANTS.PRICE_DATA_API_URL}$ticker?token=${API_CONSTANTS.PRICE_DATA_API_TOKEN}", "", COMMON.GET)
            val responseJSON = JSONObject(response.substring(1, response.length-1))
            val lastPrice = BigDecimal.valueOf(responseJSON.get("tngoLast") as Double)
            val prevClose = BigDecimal.valueOf(responseJSON.get("prevClose") as Double)
            val p_l = ((lastPrice - prevClose)/prevClose)
            val rounded_p_l = p_l.setScale(2, RoundingMode.FLOOR).multiply(BigDecimal.valueOf(100))

            watchlistRows.add(WatchlistItem(
                ticker as String,
                lastPrice,
                rounded_p_l,
                buy as Boolean,
            ))
        }

        // Assign updatesList to WatchlistAdapter
        val itemAdapter= WatchlistAdapter(watchlistRows, this)

        // Set the LayoutManager that this RecyclerView will use.
        val recyclerView: RecyclerView =view.findViewById(R.id.watchlist_recyclerview)
        recyclerView.layoutManager = LinearLayoutManager(context)

        // adapter instance is set to the recyclerview to inflate the items.
        recyclerView.adapter = itemAdapter
    }
}