package com.example.ece452_onlystocks.data

data class Entity(
    val country: String,
    val exchange: String,
    val exchange_long: String,
    val highlights: List<Highlight>,
    val industry: String,
    val match_score: Double,
    val name: String,
    val sentiment_score: Double,
    val symbol: String,
    val type: String
)