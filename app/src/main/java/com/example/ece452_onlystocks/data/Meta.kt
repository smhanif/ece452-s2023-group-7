package com.example.ece452_onlystocks.data

data class Meta(
    val found: Int,
    val limit: Int,
    val page: Int,
    val returned: Int
)