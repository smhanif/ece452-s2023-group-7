package com.example.ece452_onlystocks.data

data class Stock(
    val Ticker: String,
    val Company: String
)