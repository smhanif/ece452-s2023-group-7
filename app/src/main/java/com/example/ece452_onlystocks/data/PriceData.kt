package com.example.ece452_onlystocks.data

data class PriceData(
    val askPrice: Any,
    val askSize: Any,
    val bidPrice: Any,
    val bidSize: Any,
    val high: Double,
    val last: Double,
    val lastSaleTimestamp: String,
    val lastSize: Any,
    val low: Double,
    val mid: Any,
    val `open`: Double,
    val prevClose: Double,
    val quoteTimestamp: String,
    val ticker: String,
    val timestamp: String,
    val tngoLast: Double,
    val volume: Int
)