package com.example.ece452_onlystocks.data

data class Highlight(
    val highlight: String,
    val highlighted_in: String,
    val sentiment: Double
)