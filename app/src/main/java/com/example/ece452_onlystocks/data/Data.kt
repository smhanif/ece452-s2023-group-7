package com.example.ece452_onlystocks.data

data class Data(
    val description: String,
    val entities: List<Entity>,
    val image_url: String,
    val keywords: String,
    val language: String,
    val published_at: String,
    val relevance_score: Double,
    val similar: List<Any>,
    val snippet: String,
    val source: String,
    val title: String,
    val url: String,
    val uuid: String
)