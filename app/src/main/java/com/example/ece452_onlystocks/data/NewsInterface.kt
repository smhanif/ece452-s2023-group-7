package com.example.ece452_onlystocks.data

import com.google.gson.annotations.SerializedName
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


public interface NewsInterface {

    @GET("news/all")
    fun getNews(
        @Query("api_token") token:String,
        @Query("symbols") tickers: String,
        @Query("entity_types") entity: String

    ): Call<NewsData>

}

