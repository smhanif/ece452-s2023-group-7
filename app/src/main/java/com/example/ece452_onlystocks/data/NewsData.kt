package com.example.ece452_onlystocks.data

data class NewsData(
    val `data`: List<Data>,
    val meta: Meta
)