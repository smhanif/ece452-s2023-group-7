package com.example.ece452_onlystocks.survey

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.example.ece452_onlystocks.R

class SurveyInvestorType : AppCompatActivity() {

    private lateinit var prevButton: Button
    private lateinit var nextButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.survey_investor_type)

        prevButton = findViewById(R.id.prevButton)
        nextButton = findViewById(R.id.nextButton)

        prevButton.setOnClickListener {
            // Proceed to the prev activity
            val intent = Intent(this, SurveyInvestorLevel::class.java)
            startActivity(intent)
        }

        nextButton.setOnClickListener {
            // Proceed to the next activity or perform necessary actions
            val intent = Intent(this, SurveyRiskTolerance::class.java)
            startActivity(intent)
        }
    }
}
