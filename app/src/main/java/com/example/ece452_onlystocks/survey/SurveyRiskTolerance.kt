package com.example.ece452_onlystocks.survey

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.example.ece452_onlystocks.MainActivity
import com.example.ece452_onlystocks.R

class SurveyRiskTolerance : AppCompatActivity() {

    private lateinit var prevButton: Button
    private lateinit var submitButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.survey_risk_tolerance)

        prevButton = findViewById(R.id.prevButton)
        submitButton = findViewById(R.id.submitButton)

        prevButton.setOnClickListener {
            // Proceed to the prev activity
            val intent = Intent(this, SurveyInvestorType::class.java)
            startActivity(intent)
        }

        submitButton.setOnClickListener {
            // Proceed to the next activity or perform necessary actions
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }
}
