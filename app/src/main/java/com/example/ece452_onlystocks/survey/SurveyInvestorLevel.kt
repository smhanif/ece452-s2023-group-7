package com.example.ece452_onlystocks.survey

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.example.ece452_onlystocks.R

class SurveyInvestorLevel : AppCompatActivity() {

    private lateinit var nextButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.survey_investor_level)

        nextButton = findViewById(R.id.nextButton)

        nextButton.setOnClickListener {
            // Proceed to the next activity or perform necessary actions
            val intent = Intent(this, SurveyInvestorType::class.java)
            startActivity(intent)
        }
    }
}
