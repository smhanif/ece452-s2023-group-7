package com.example.ece452_onlystocks

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import at.favre.lib.crypto.bcrypt.BCrypt
import com.example.ece452_onlystocks.survey.SurveyInvestorLevel
import com.example.ece452_onlystocks.util.APIRequest
import java.security.SecureRandom

class SignUpActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val fname = findViewById<EditText>(R.id.name)
        val lname = findViewById<EditText>(R.id.lastname)
        val age = findViewById<EditText>(R.id.age)
        val emailInput = findViewById<EditText>(R.id.email)
        val username = findViewById<EditText>(R.id.username)
        val password = findViewById<EditText>(R.id.password)
        val confirm = findViewById<EditText>(R.id.confirm)
        val bio = findViewById<EditText>(R.id.bio)
        val twitter = findViewById<EditText>(R.id.reddit_username)
        val reddit = findViewById<EditText>(R.id.twitter_username)
        val signUpBtn = findViewById<Button>(R.id.signup)


        signUpBtn.setOnClickListener {
            val fnameStr = fname.text.toString().trim()
            val lnameStr = lname.text.toString().trim()
            val emailInputStr = emailInput.text.toString().trim().lowercase()
            val ageStr = age.text.toString().trim()
            val usernameStr = username.text.toString().trim().lowercase()
            val passwordStr = password.text.toString()
            val confirmStr = confirm.text.toString()
            val bioStr = bio.text.toString().trim()
            val twitterStr = twitter.text.toString().trim()
            val redditStr = reddit.text.toString().trim()

            if(fnameStr.isEmpty()){
                fname.error = "First name required"
                return@setOnClickListener
            }
            else if(lnameStr.isEmpty()){
                lname.error = "Last name required"
                return@setOnClickListener
            }
            else if(ageStr.toInt() < 18){
                age.error = "Must be 18+ to create an account"
                return@setOnClickListener
            }
            else if(!isEmailValid(emailInputStr.trim())){
                emailInput.error = "Email is wrong format"
                return@setOnClickListener
            }
            else if(usernameStr.isEmpty()){
                username.error = "Username required"
                return@setOnClickListener
            }
            else if(passwordStr.isEmpty()){
                password.error = "Password required"
                return@setOnClickListener
            }
            else if(confirmStr.isEmpty()){
                confirm.error = "Confirm password required"
                return@setOnClickListener
            }
            else if(passwordStr != confirmStr)
            {
                confirm.error = "Passwords must match!"
                password.error = "Passwords must match!"
                return@setOnClickListener
            }
            else{
                println("Testing... 1 2 3")

                var userOrEmailExists = false

                val tableName = "user"
                val primaryKey = "userName"
                val primaryKeyValue = usernameStr
                val usernameEndpoint = "https://pka2s6gf57.execute-api.ca-central-1.amazonaws.com/v1/test?table=$tableName&$primaryKey=$primaryKeyValue"

                val usernameResponse = APIRequest(usernameEndpoint, "", "GET")
                println(usernameResponse)

                if (usernameResponse != "{}") {
                    username.error = "Username taken. Please choose another username"
                    userOrEmailExists = true
                }

                val emailKey = "email"
                val emailKeyValue = emailInputStr
                val emailEndpoint = "https://pka2s6gf57.execute-api.ca-central-1.amazonaws.com/v1/test?table=$tableName&$emailKey=$emailKeyValue"

                val emailResponse = APIRequest(emailEndpoint, "", "GET")
                println(emailResponse)

                if (emailResponse != "{}") {
                    emailInput.error = "Email already in use. Please use another email"
                    userOrEmailExists = true
                }

                if (userOrEmailExists) {
                    return@setOnClickListener
                }

                val random = SecureRandom()
                val salt = ByteArray(16)
                random.nextBytes(salt)

                val passwordHash =
                    BCrypt.withDefaults().hashToString(12, passwordStr.plus(salt).toCharArray())

                val postEndpoint =
                    "https://pka2s6gf57.execute-api.ca-central-1.amazonaws.com/v1/test?table=$tableName"

                println(bioStr)
                println(redditStr)
                println(twitterStr)

                val requestBody = """
                {
                  "userName": {"S": "$usernameStr"},
                  "Password": {"S": "$passwordHash"},
                  "PasswordSalt": {"S": "$salt"},
                  "FirstName": {"S": "$fnameStr"},
                  "LastName": {"S": "$lnameStr"},
                  "email": {"S": "$emailInputStr"},
                  "picture": {"B": "base64-encoded-image-data"},
                  "bio": {"S": "$bioStr"},
                  "redditUserName": {"S": "$redditStr"},
                  "twitterUserName": {"S": "$twitterStr"},
                  "followers": {"L": []},
                  "following": {"L": []}
                }
                """.trimIndent()

                val response = APIRequest(postEndpoint, requestBody, "POST")
                println(response)
                println("Testing... 4 5 6")

                val sharedPref = applicationContext.getSharedPreferences("MyPref", MODE_PRIVATE)
                with (sharedPref.edit()) {
                    putString("username", usernameStr)
                    apply()
                }

                val intent = Intent(this, SurveyInvestorLevel::class.java)
                startActivity(intent)
            }

        }
    }

    fun isEmailValid(email: String): Boolean {
        val emailRegex = Regex("[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}")
        return emailRegex.matches(email)
    }
}


