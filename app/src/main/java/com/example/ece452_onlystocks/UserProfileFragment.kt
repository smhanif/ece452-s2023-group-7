package com.example.ece452_onlystocks

import android.content.Context
import android.content.Intent
import android.media.Image
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ece452_onlystocks.constants.API_CONSTANTS
import com.example.ece452_onlystocks.constants.COMMON
import com.example.ece452_onlystocks.constants.DB_CONSTANTS
import com.example.ece452_onlystocks.news.WatchlistAdapter
import com.example.ece452_onlystocks.util.APIRequest
import com.example.ece452_onlystocks.util.getCommonFollowers
import com.example.ece452_onlystocks.util.getCommonStocks
import com.example.ece452_onlystocks.util.getUser
import com.example.ece452_onlystocks.util.isFollowing
import com.example.ece452_onlystocks.util.updateFollowerFollowing
import com.example.ece452_onlystocks.watchlist.WatchlistItem
import org.json.JSONArray
import org.json.JSONObject
import java.math.BigDecimal
import java.math.RoundingMode


class UserProfileFragment : Fragment() {

    private lateinit var followUnfollowButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_user_profile, container, false)
        val userObj = getUser(this.arguments?.getString("user", "user"))

        val fullName = userObj.get("FirstName") as String + " " + userObj.get("LastName") as String
        view.findViewById<TextView>(R.id.profile_name).text = fullName
        view.findViewById<TextView>(R.id.their_watchlist).text = fullName + "'s Watchlist"

        view.findViewById<TextView>(R.id.followers_count).text = (userObj.get("followers") as JSONArray).length().toString()
        view.findViewById<TextView>(R.id.following_count).text = (userObj.get("following") as JSONArray).length().toString()
        val sharedPref = activity?.applicationContext?.getSharedPreferences("MyPref", Context.MODE_PRIVATE)
        val userName = sharedPref?.getString("username", null)
        val mutualFollowers = getCommonFollowers(this.arguments?.getString("user", "user"),userName)
        val commonStocks = getCommonStocks(this.arguments?.getString("user", "user"), userName)
        val thingsInCommon = "$mutualFollowers mutual followers, $commonStocks shared stocks"
        view.findViewById<TextView>(R.id.profile_things_in_common).text = thingsInCommon

        // bio
        val bio = userObj.get("bio") as? String
        view.findViewById<TextView>(R.id.profile_bio).text = bio

        // twitter link
        val twitterUserName = userObj.get("twitterUserName") as? String
        val twitterLink = "https://www.twitter.com/$twitterUserName"
        val twitterButton: ImageButton = view.findViewById(R.id.twitter_profile_link)
        twitterButton.setOnClickListener {
            goToLink(twitterLink)
        }

        // reddit link
        val redditUserName = userObj.get("redditUserName") as? String
        val redditLink = "https://www.reddit.com/user/$redditUserName"
        val redditButton: ImageButton = view.findViewById(R.id.reddit_profile_link)
        redditButton.setOnClickListener {
            goToLink(redditLink)
        }

        // follow or unfollow button
        followUnfollowButton = view.findViewById(R.id.follow_button)
        followUnfollowButton.text = "+ Follow"
        val following: Boolean = isFollowing(userName, this.arguments?.getString("user", "user"))
        if (following) {
            followUnfollowButton.text = "Unfollow"
        }

        followUnfollowButton.setOnClickListener {
            if (following) {
                followUnfollowButton.text = "+ Follow"
                updateFollowerFollowing(userName, this.arguments?.getString("user", "user"), false)
            } else {
                followUnfollowButton.text = "Unfollow"
                updateFollowerFollowing(userName, this.arguments?.getString("user", "user"), true)
            }
            parentFragmentManager.beginTransaction().detach(this).commit ()
            parentFragmentManager.beginTransaction().attach(this).commit ()
        }

        // go back button
        val SearchUserPage = SearchUserPage()
        view.findViewById<ImageButton>(R.id.go_back).setOnClickListener{
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(((view as ViewGroup).parent as View).id, SearchUserPage)
                ?.addToBackStack(null)
                ?.commit()
        }

        //get watchlist items for current user
        val table = DB_CONSTANTS.WATCHLIST_TABLE
        val baseURL = DB_CONSTANTS.BASE_URL
        val otherUserName = userObj.get("userName") as String
        var endpoint = "$baseURL?table=$table&userName=$otherUserName"
        val resp = APIRequest(endpoint, "", COMMON.GET)

        val respJSON = JSONArray(resp)

        val watchlistRows=ArrayList<WatchlistItem>()

        for (i in 0 until respJSON.length()) {

            // store each object in JSONObject
            val item: JSONObject = respJSON.getJSONObject(i)
            val ticker = item.get("ticker")
            val buy = item.get("buy")
            val response = APIRequest("${API_CONSTANTS.PRICE_DATA_API_URL}$ticker?token=${API_CONSTANTS.PRICE_DATA_API_TOKEN}", "", COMMON.GET)
            val responseJSON = JSONObject(response.substring(1, response.length-1))
            val lastPrice = BigDecimal.valueOf(responseJSON.get("tngoLast") as Double)
            val prevClose = BigDecimal.valueOf(responseJSON.get("prevClose") as Double)
            val p_l = ((lastPrice - prevClose)/prevClose)
            val rounded_p_l = p_l.setScale(2, RoundingMode.FLOOR).multiply(BigDecimal.valueOf(100))

            watchlistRows.add(
                WatchlistItem(
                ticker as String,
                lastPrice,
                rounded_p_l,
                buy as Boolean,
            )
            )
        }

        // Assign updatesList to WatchlistAdapter
        val itemAdapter= WatchlistAdapter(watchlistRows, this)

        // Set the LayoutManager that this RecyclerView will use.
        val recyclerView: RecyclerView =view.findViewById(R.id.watchlist_recyclerview)
        recyclerView.layoutManager = LinearLayoutManager(context)

        // adapter instance is set to the recyclerview to inflate the items.
        recyclerView.adapter = itemAdapter

        return view
    }

    private fun goToLink(url: String) {
        val uriUrl = Uri.parse(url)
        val launchBrowser = Intent(Intent.ACTION_VIEW, uriUrl)
        startActivity(launchBrowser)
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            UserProfileFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}